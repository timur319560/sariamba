const { Router } = require("express");
const contactControler = require("./Contact.controler");

const contactRouter = Router();
contactRouter.post(
  "/",
  contactControler.validateCreateContact,
  contactControler.createContact,
);
contactRouter.get("/", contactControler.getContacts);
contactRouter.get("/:id", contactControler.getContact);
contactRouter.delete("/:id", contactControler.deleteContact);

contactRouter.put(
  "/:id",
  contactControler.validateUpdateContact,
  contactControler.updateContact,
);
contactRouter.get("/auth/register", (req, res) => {
  console.log("object");
  // return res.status(200);
});
module.exports = contactRouter;
