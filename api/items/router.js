const { Router } = require("express");
const ContactControler = require("./controler");
const contactRouter = Router();

contactRouter.post(
  "/api/contacts",
  ContactControler.validateCreateContact,
  ContactControler.createContact,
);

contactRouter.get(
  "/users/current",
  ContactControler.authorize,
  ContactControler.getCurrentContact,
);

contactRouter.get("/", ContactControler.getContacts);
contactRouter.get(
  "/api/contacts/:id",
  ContactControler.validateId,
  ContactControler.getContact,
);
contactRouter.patch(
  "/auth/logout",
  ContactControler.authorize,
  ContactControler.logout,
);
contactRouter.delete(
  "/api/contacts/:id",
  ContactControler.validateId,
  ContactControler.deleteContact,
);

contactRouter.post(
  "/auth/register",
  ContactControler.validateCreateContact,
  ContactControler.register,
);
contactRouter.put(
  "/auth/login",
  ContactControler.validateSignIn,
  ContactControler.login,
);
contactRouter.put(
  "/api/contacts/:id",
  ContactControler.validateId,
  ContactControler.validateUpdateContact,
  ContactControler.updateContact,
);

module.exports = contactRouter;
