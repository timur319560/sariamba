const Joi = require("joi");
const contactModel = require("./model");
const jwt = require("jsonwebtoken");
const _ = require("lodash");
const bcryptjs = require("bcryptjs");
const { UnauthorizedError } = require("../helpers/errors");
const {
  Types: { ObjectId },
} = require("mongoose");
class ContactControler {
  constructor() {
    this._costFactor = 4;
  }

  // GET CURRENT CONTACT/USER
  get getCurrentContact() {
    return this._getCurrentContact.bind(this);
  }

  async _getCurrentContact(req, res, next) {
    console.log(req.contact);
    const [contactForResponse] = this.prepareContactsResponse([req.contact]);
    return res.status(200).json(contactForResponse);
  }

  // GET CONTACTS
  get getContacts() {
    return this._getContacts.bind(this);
  }

  async _getContacts(req, res, next) {
    const contacts = await contactModel.find();
    return res.status(200).json(this.prepareContactsResponse(contacts));
  }

  async _getContacts(req, res, next) {
    const contacts = await contactModel.find();
    return res.status(200).json(this.prepareContactsResponse(contacts));
  }
  // GET CONTACT

  get getContact() {
    return this._getContact.bind(this);
  }

  async _getContact(req, res, next) {
    const contactId = req.params.id;

    const contact = await contactModel.findById(contactId);
    if (!contact) {
      return res.status(404).json({ message: "Not found" });
    }

    const [contactForResponse] = this.prepareContactsResponse([contact]);
    return res.status(200).json(contactForResponse);
  }

  // CREATE CONTACT
  get _createContact() {
    return this.createContact.bind(this);
  }
  get deleteContact() {
    return this._deleteContact.bind(this);
  }

  async createContact(req, res, next) {
    try {
      const { password, email, phone, subscription, token, name } = req.body;
      const existingContact = await contactModel.findOne({ email });
      if (existingContact) {
        return res.status(409).json({ message: "Email in use" });
      }
      const passwordHash = await bcryptjs.hash(password, this._costFactor);
      const contact = await contactModel.create({
        email,
        name,
        phone,
        token,
        subscription,
        password: passwordHash,
      });
      const created = await contactModel.create(contact);
      return res.status(201).json({
        id: created._id,
        name: created.name,
        email: created.email,
      });
    } catch (err) {
      console.log(err);
    }
  }

  validateCreateContact(req, res, next) {
    const createContactRules = Joi.object({
      name: Joi.string().required(),
      email: Joi.string().required(),
      phone: Joi.string().required(),
      subscription: Joi.string().required(),
      password: Joi.string().required(),
      token: Joi.string().allow("").allow(null),
    });

    const result = Joi.validate(req.body, createContactRules);
    if (result.error) {
      return res.status(400).json({
        message: "Missing required name field",
      });
    }
    next();
  }

  
  // REGISTER

  get register() {
    return this._register.bind(this);
  }

  async _register(req, res, next) {
    try {
      const { password, email, phone, subscription, token, name } = req.body;
      const existingContact = await contactModel.findOne({ email });
      if (existingContact) {
        return res.status(409).json({ message: "Email in use" });
      }
      const passwordHash = await bcryptjs.hash(password, this._costFactor);
      const contact = await contactModel.create({
        email,
        name,
        phone,
        token,
        subscription,
        password: passwordHash,
      });
      const created = await contactModel.create(contact);
      if (created) {
        const token = jwt.sign({ id: created._id }, process.env.JWT_SECRET, {
          expiresIn: 2 * 24 * 60 * 60,
        });
        await contactModel.updateToken(created._id, token);
      }

      return res.status(201).json({
        id: created._id,
        name: created.name,
        email: created.email,
      });
    } catch (err) {
      console.log(err);
    }
  }
  // LOGIN
  async login(req, res, next) {
    try {
      const { email, password } = req.body;
      const user = await contactModel.findContactByEmail(email);
      if (!user) {
        return res.status(401).send("You drunk bruv,! innit?!?!?!");
      }

      const passwordValidation = await bcryptjs.compare(
        password,
        user.password,
      );
      if (!passwordValidation) {
        return res.status(401).send("You drunk bruv, innit?!?!?!");
      }

      const token = jwt.sign({ id: user._id }, process.env.JWT_SECRET, {
        expiresIn: 2 * 24 * 60 * 60,
      });
      await contactModel.updateToken(user._id, token);

      return res.status(200).json({ token });
    } catch (err) {
      console.log(err);
    }
  }

  // VALIDATE LOGIN
  validateSignIn(req, res, next) {
    const signInRules = Joi.object({
      email: Joi.string().required(),
      password: Joi.string().required(),
    });

    const validationResult = Joi.validate(req.body, signInRules);
    if (validationResult.error) {
      return res.status(400).send(validationResult.error);
    }

    next();
  }

  async logout(req, res, next) {
    console.log(req.contact);
    try {
      const contact = req.contact;
      await contactModel.updateToken(contact._id, null);
      return res.status(203).send();
    } catch (err) {
      next(err);
    }
  }

  async authorize(req, res, next) {
    try {
      const authorizationHeader = req.get("Authorization") || "";
      const token = authorizationHeader.replace("Bearer ", "");

      let contactId;
      try {
        contactId = await jwt.verify(token, process.env.JWT_SECRET).id;
      } catch (err) {
        return res.status(401).json({ message: "Not authorized" });
      }

      const contact = await contactModel.findById(contactId);

      if (!contact || contact.token !== token) {
        throw new UnauthorizedError();
      }

      req.contact = contact;
      req.token = token;
      next();
    } catch (err) {
      return res.status(401).json({ message: "Not authorized" });
    }
  }

  validateId(req, res, next) {
    const id = req.params.id;

    if (!ObjectId.isValid(id)) {
      return res.status(400).send("Are you drunk bruv?");
    }

    next();
  }
  async _deleteContact(req, res, next) {
    const contactId = req.params.id;
    console.log(contactId);
    const deleteContact = await contactModel.findByIdAndDelete(contactId);

    if (!deleteContact) {
      return res.status(404).json({ message: "Not found" });
    }
    return res.status(200).json({
      message: "contact deleted",
    });
  }

  prepareContactsResponse(contacts) {
    return contacts.map(contact => {
      const { _id, name, email, phone, subscription } = contact;
      return { id: _id, name, email, phone, subscription };
    });
  }

  validateUpdateContact() {
    const updateContactRules = Joi.object({
      name: Joi.string(),
      email: Joi.string(),
      phone: Joi.string(),
      subscription: Joi.string(),
      password: Joi.string(),
      token: Joi.string(),
    });

    const bodyLength = Object.keys(req.body).length;
    if (bodyLength === 0) {
      return res.status(400).send({
        message: "missing fields",
      });
    }
    const result = updateContactRules.validate(req.body);
    if (result.error) {
      return res.status(400).send({
        message: "unvalid format of data",
      });
    }
    next();
  }

  updateContact() {}
}

module.exports = new ContactControler();
